<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Repository\OperationRepository;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Operation;

class OperationController extends Controller
{
    private $serializer;

    public function __construct()
    {
        $encoder = new JsonEncoder();
        $normalizer = new ObjectNormalizer();
        $normalizer->setCircularReferenceLimit(1);
        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });
        $this->serializer = new Serializer([$normalizer], [$encoder]);
    }

    /**
     * @Route("/operation", name="operation", methods={"GET"})
     */
    public function all(OperationRepository $repo)
    {
        $list = $repo->findAll();

        $data = $this->serializer->normalize($list, null, ['attributes' => ['id', 'sum', 'description', 'date', 'tags', 'type', 'budgets', 'total','totalInput','totalOutput']]);

        $response = new Response($this->serializer->serialize($data, 'json'));
        return $response;
    }

    /**
     * @Route("/total", name="total", methods={"GET"})
     */
    public function getTotal(OperationRepository $repo)
    {
        $data = $repo->getTotal();
        $response = new Response($this->serializer->serialize($data[0], 'json'));
        return $response;
    }

    /**
     * @Route("/total-input", name="total-input", methods={"GET"})
     */
    public function getTotalInput(OperationRepository $repo)
    {
        $data = $repo->getTotalInput();
        $response = new Response($this->serializer->serialize($data[0], 'json'));
        return $response;
    }

    /**
     * @Route("/total-output", name="total-total", methods={"GET"})
     */
    public function getTotalOutput(OperationRepository $repo)
    {
        $data = $repo->getTotalOutput();
        $response = new Response($this->serializer->serialize($data[0], 'json'));
        return $response;
    }

    /**
     * @Route("/operation", name="new_operation", methods={"POST"})
     */

    public function addOperation(Request $request)
    {
        $manager = $this->getDoctrine()->getManager();

        $content = $request->getContent();
        $operation = $this->serializer->deserialize($content, Operation::class, "json");

        $manager->persist($operation);
        $manager->flush();

        $data = $this->serializer->normalize($operation, null, ['attributes' => ['id', 'sum', 'description', 'date', 'tags', 'type', 'budgets']]);

        $response = new Response($this->serializer->serialize($operation, "json"));
        return $response;
    }

    /**
     * @Route("/operation/{id}", name="delete_operation", methods={"DELETE"})
     */
    public function delete(Operation $operation)
    {
        $manager = $this->getDoctrine()->getManager();

        $manager->remove($operation);
        $manager->flush();

        return new Response("OK", 204);
    }

    /**
     * @Route("/operation/{id}", name="update_operation", methods={"PUT"})
     */
    public function update(Request $request, Operation $operation)
    {
        $manager = $this->getDoctrine()->getManager();

        $content = $request->getContent();
        $update = $this->serializer->deserialize($content, Operation::class, "json");

        $operation->setSum($update->getSum());
        $operation->setDescription($update->getDescription());
        $operation->setTags($update->getTags());
        $operation->setType($update->getType());


        $manager->persist($operation);
        $manager->flush();

        $data = $this->serializer->normalize($operation, null, ['attributes' => ['id', 'sum', 'description', 'date', 'tags', 'type', 'budgets']]);

        $response = new Response($this->serializer->serialize($data, "json"));
        return $response;
    }
}
