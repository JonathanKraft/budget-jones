import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../service/authentication/authentication.service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username:string;
  password:string;
  feedback:string;

  isLogged:Observable<any>;
  constructor(private authService:AuthenticationService, private router:Router) { }

  ngOnInit() {
    this.isLogged = this.authService.user;
  }

  login() {
    this.authService.login(this.username, this.password)
    .subscribe(() => this.feedback = '',
    (error) => {
      if(error.status === 401) {
        this.feedback = "Mauvais identifiants";
      } else {
        this.feedback = "Erreur serveur";
      }
    });
  }

  logout() {
    this.authService.logout();
    //On fait une redirection quand on se déconnecte
    this.router.navigate(['']);
  }
}
