import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from "rxjs/operators";
import { Operation } from '../entity/operation';

@Injectable({
  providedIn: 'root'
})
export class OperationService {
  private urlSql = "http://localhost:8080";
  private url = `${this.urlSql}/operation`;


  constructor(private http: HttpClient) { }

  findAll(): Observable<Operation[]> {
    return this.http.get<Operation[]>(this.url).pipe(
      map(data => {
        for (let operation of data) {
          operation.date = new Date(operation.date * 1000);
        }
        return data;
      })
    );
  }



  findByid(id: number): Observable<Operation> {

    return this.http.get<Operation>(`${this.url}/${id}`);
  }

  add(operation: Operation): Observable<Operation> {

    return this.http.post<Operation>(this.url, operation);
  }

  delete(id: number): Observable<any> {

    return this.http.delete(`${this.url}/${id}`)
  }

  update(operation: Operation): Observable<Operation> {
    return this.http.put<Operation>(`${this.url}/${operation.id}`, operation);
  }

  getTotal(): Observable<any> {
    return this.http.get(`${this.urlSql}/total`);
  }

  getTotalInput(): Observable<any> {
    return this.http.get(`${this.urlSql}/total-input`);
  }

  getTotalOutput(): Observable<any> {
    return this.http.get(`${this.urlSql}/total-output`);
  }

  /**
   * findAll():Operation[],
   * add():Operation,
   * delete():any,
   * addition(a:number ,b:number):number,
   */
}
