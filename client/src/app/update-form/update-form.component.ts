import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { Operation } from '../entity/operation';

@Component({
  selector: 'app-update-form',
  templateUrl: './update-form.component.html',
  styleUrls: ['./update-form.component.css']
})
export class UpdateFormComponent implements OnInit {

  @Input() updated: Operation;
  @Input() tagsUpdate: string;
  @Output() submit = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
  }

  onClick() {
    this.updated.tags= this.tagsUpdate.match(/\w+/g)
    this.submit.emit(this.updated);

    
    
  }
}
